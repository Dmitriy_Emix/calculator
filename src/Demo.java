import java.util.Scanner;

/**
 * @author Эмих Дмитрий.
 */
public class Demo {
    public static void main(String[] args) {
        String str = input();
        if (!checkInput(str)) {
            System.exit(1);
        }
    }

    /**
     * Вводим исходные данные (2 операнда и знак операции)
     *
     * @return возвращает введённую строку.
     */
    public static String input() {
        Scanner reader = new Scanner(System.in);
        String str = reader.nextLine();
        reader.close();
        return str;
    }

    /**
     * Проверка корректного ввода на исходных данные.
     *
     * @param str строка
     * @return возвращает Булево значение.
     */
    public static boolean checkInput(String str) {
        // Разбиваем строку после пробела на массив строк.
        String[] inputData = str.split(" ");
        // Проверяем ввод полной строки
        if (str.length() == 0) {
            throw new NullPointerException("Пустая строка!");
        }
        if (inputData.length != 3) {
            System.out.println("Данные введены не полностью!");
            return false;
        }
        // Проверяем на соответствие типа Double
        try {
            double op1 = Double.parseDouble(inputData[0]);
            double op2 = Double.parseDouble(inputData[1]);
        } catch (NumberFormatException e) {
            System.out.println("Неверные значения операндов! " + e);
            return false;
        }
        // Преобразуем тип из String в char
        char[] selection = inputData[2].toCharArray();
        char[] sign = {'+', '-', '*', '/', '%', '^', '#'};
        boolean flag = true;
        // Проверяем на ввод знака операции
        for (int i = 0; i < sign.length; i++) {
            if (sign[i] == selection[0]) {
                flag = false;
                break;
            }
        }
        if (flag) {
            System.out.println("Неверный знак операции!");
            return false;
        }
        return true;
    }

    private static void selectionOperation(double op1, double op2, String str) {
        int firstValueInt = 0;
        int secondValueInt = 0;
        char[] selection = str.toCharArray();
        if (op1 % 1 == 0) {
            firstValueInt = (int) op1;
        }
        if (op2 % 1 == 0) {
            secondValueInt = (int) op2;
        }
        if ((firstValueInt == 0 && secondValueInt == 0)||(firstValueInt!=0 && secondValueInt==0){
            throw new ArithmeticException("Деление на ноль невозможно!");
        }
        switch (selection[0]) {
            case '+':
                output(firstValueInt, secondValueInt, Calculator.addition(firstValueInt, secondValueInt), str);
                break;
            case '-':
                output(firstValueInt, secondValueInt, Calculator.subtraction(firstValueInt, secondValueInt), str);
                break;
            case '*':
                output(firstValueInt, secondValueInt, Calculator.multiplication(firstValueInt, secondValueInt), str);
                break;
            case '/':
                output(firstValueInt, secondValueInt, Calculator.div(firstValueInt, secondValueInt), str);
                break;
            case '%':
                output(firstValueInt, secondValueInt, Calculator.dev(firstValueInt, secondValueInt), str);
                break;
            case '^':
                output(firstValueInt, secondValueInt, Calculator.power(firstValueInt, secondValueInt), str);
                break;
            case '#':
                output(firstValueInt, secondValueInt, (int) Calculator.sqrt(firstValueInt), str);
                break;
        }
    }
// else {
//            switch (selection[0]) {
//                case '+':
//                    output(op1, op2, Calculator.addition(op1, op2), str);
//                    break;
//                case '-':
//                    output(op1, op2, Calculator.subtraction(op1, op2), str);
//                    break;
//                case '*':
//                    output(op1, op2, Calculator.multiplication(op1, op2), str);
//                    break;
//                case '/':
//                    output(op1, op2, Calculator.div(op1, op2), str);
//                    break;
//                case '^':
//                    output(op1, op2, Calculator.power(op1, secondValueInt), str);
//                    break;
//                case '#':
//                    output(op1, op2, Calculator.sqrt(op1), str);
//                    break;
//            }
//        }
//    }

    public static void output(int firstValue, int secondValue, int result, String str) {
        System.out.println(firstValue + " " + str + " " + secondValue + " = " + result);
    }

    public static void output(double firstValue, double secondValue, double result, String str) {
        System.out.println(firstValue + " " + str + " " + secondValue + " = " + result);
    }
}