/**
 * Created by Дмитрий on 14.09.2016.
 */
class Calculator {
    public static double addition(double firstNum, double secondNum) {    //Высчитывание суммы
        double r = firstNum + secondNum;
        if (Double.MAX_VALUE<r || Double.MIN_VALUE > r){
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return r;
    }

    public static double subtraction(double firstNum, double secondNum) {   //Высчитывание вычитания
        double r = firstNum - secondNum;
        if (Double.MAX_VALUE < r || Double.MIN_VALUE > r) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return r;
    }

    public static double multiplication(double firstNum, double secondNum)  //Высчитывание умножения
    {
        double r = firstNum * secondNum;
        if (Double.MAX_VALUE < r || Double.MIN_VALUE >r) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return r;
    }
    public static double div(double firstNum, double secondNum)        //Высчитывание вещественного деления
    {
        double r = firstNum/secondNum;
        if (Double.MAX_VALUE<firstNum || Double.MIN_VALUE>secondNum){
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return r;
    }
    public static int div(int firstNum, int secondNum){            //Целочисленное деление
        int r = firstNum/secondNum;
        if (Double.MAX_VALUE < r || Double.MIN_VALUE > r){
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return r;
    }
    public static double dev(double firstNum, double secondNum) {
        double r = firstNum%secondNum;
        if (Double.MAX_VALUE<firstNum || Double.MIN_VALUE>secondNum){
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return r;
    }


    public static double power(double value, int degree) {
        double result = 1;
        int degree2 = 0;

        if (degree==0){
            return 1;
        }

        if (degree<0){
            degree2 = degree;
            degree *= -1;
        }
        for (int i=0; i<degree; i++){
            result *= value;
        }
        if (degree2<0){
            return 1/result;
        }
        else {
            return result;
        }
    }

    public static double sqrt(double firstValueInt) {
        return 0;
    }